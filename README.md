# Brewery
A simple Angular application containing Rest Controller and Angular Material components.

Schema for DB is based on script from:
https://www.openbrewerydb.org/

# Build
Built project is stored in dist/ directory and run by `ng build`.

# Development server
Run by `ng serve` and navigate to `http://localhost:4200/`.