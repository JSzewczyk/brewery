import { Component, OnInit } from '@angular/core';
import {BreweryService} from '../../commons/requests/brewery.service';
import {Brewery} from '../../commons/models/brewery';

@Component({
  selector: 'app-brewery',
  templateUrl: './brewery.component.html',
  styleUrls: ['./brewery.component.scss']
})
export class BreweryComponent implements OnInit {

  constructor() { }

  ngOnInit(): void { }
}
