import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BreweryComponent} from './brewery.component';
import { BreweryTableComponent } from './brewery-table/brewery-table.component';
import {MatTableModule} from '@angular/material/table';
import { BreweryFilterComponent } from './brewery-filter/brewery-filter.component';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule} from '@angular/forms';
import { BreweryDetailsComponent } from './brewery-details/brewery-details.component';
import {MatButtonModule} from '@angular/material/button';

const materialModules = [
  MatTableModule,
  MatSelectModule,
  FormsModule,
  MatButtonModule
];

@NgModule({
  declarations: [
    BreweryComponent,
    BreweryTableComponent,
    BreweryFilterComponent,
    BreweryDetailsComponent
  ],
  imports: [
    CommonModule,
    materialModules
  ]
})
export class BreweryModule { }
