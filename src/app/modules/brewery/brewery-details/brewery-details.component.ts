import {Component, OnDestroy, OnInit} from '@angular/core';
import {Brewery} from '../../../commons/models/brewery';
import {BreweryService} from '../../../commons/requests/brewery.service';
import {BreweryDetailsService} from '../../../commons/services/brewery-details.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-brewery-details',
  templateUrl: './brewery-details.component.html',
  styleUrls: ['./brewery-details.component.scss']
})
export class BreweryDetailsComponent implements OnInit, OnDestroy {

  public breweries: Brewery[] = [];
  public hideLoadMore = true;
  private selectedBreweryType = '';
  private breweryPageNumber = 15;
  private loopCount = 2;
  private $destroy = new Subject();

  constructor(
    private breweryService: BreweryService,
    private breweryDetailsService: BreweryDetailsService
  ) {
  }

  ngOnInit(): void {
    this.getBreweryPageNumber();
  }

  public selectedType(val: string): void {
    this.selectedBreweryType = val;
    this.getBreweries(this.selectedBreweryType, this.breweryPageNumber);
  }

  public loadMore(): void {
    const page = this.loopCount * this.breweryPageNumber;
    this.getBreweries(this.selectedBreweryType, page);
    this.loopCount++;
  }

  private getBreweries(type: string, page: number): void {
    this.breweryService.getBreweries(type, page).pipe(takeUntil(this.$destroy)).subscribe((val: Brewery[]) => {
      this.breweries = val;
      this.controlLoadMore();
    });
  }

  private getBreweryPageNumber(): void {
    this.breweryDetailsService.getBreweryPageNumber().pipe(takeUntil(this.$destroy)).subscribe((val: number) => {
      this.breweryPageNumber = val;
    });
  }

  private controlLoadMore(): void {
    const num = (this.breweries.length % this.breweryPageNumber);
    this.hideLoadMore = num > 0;
  }

  ngOnDestroy(): void {
    this.$destroy.next();
  }
}
