import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BreweryTypeEnum} from '../../../commons/models/enums/brewery-type.enum';
import {MatSelectChange} from '@angular/material/select';


@Component({
  selector: 'app-brewery-filter',
  templateUrl: './brewery-filter.component.html',
  styleUrls: ['./brewery-filter.component.scss']
})
export class BreweryFilterComponent implements OnInit {

  public readonly breweriesType: string[] = [
    BreweryTypeEnum.MICRO,
    BreweryTypeEnum.NANO,
    BreweryTypeEnum.REGIONAL,
    BreweryTypeEnum.BREWPUB,
    BreweryTypeEnum.LARGE,
    BreweryTypeEnum.PLANNING,
    BreweryTypeEnum.BAR,
    BreweryTypeEnum.CONTRACT,
    BreweryTypeEnum.PROPRIETOR,
    BreweryTypeEnum.CLOSED,
  ];

  @Output() public selectedType: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  public selectionChange(val: MatSelectChange): void {
    this.selectedType.emit(val.value);
  }

}
