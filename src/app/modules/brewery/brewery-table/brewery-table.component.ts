import {Component, Input, OnInit} from '@angular/core';
import {Brewery} from '../../../commons/models/brewery';
import {MatTableDataSource} from '@angular/material/table';
import {TableColumnsEnum} from '../../../commons/models/enums/table-columns.enum';

@Component({
  selector: 'app-brewery-table',
  templateUrl: './brewery-table.component.html',
  styleUrls: ['./brewery-table.component.scss']
})
export class BreweryTableComponent implements OnInit {

  public readonly columns: string[] = [TableColumnsEnum.NAME, TableColumnsEnum.TYPE, TableColumnsEnum.STATE];
  public readonly columnEnum = TableColumnsEnum;
  public dataSource = new MatTableDataSource<Brewery>([]);

  @Input() public set brewery(val: Brewery[]) {
    this.initDataSource(val);
  }

  constructor() { }

  ngOnInit(): void {
  }

  private initDataSource(data: Brewery[]): void {
    this.dataSource = new MatTableDataSource(data);
  }

}
