import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BreweryTableComponent } from './brewery-table.component';

describe('BreweryTableComponent', () => {
  let component: BreweryTableComponent;
  let fixture: ComponentFixture<BreweryTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BreweryTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BreweryTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
