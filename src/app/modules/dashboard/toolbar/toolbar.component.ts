import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {OptionsDialogComponent} from './options-dialog/options-dialog.component';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  public openDialog(): void {
    const dialogRef = this.dialog.open(
      OptionsDialogComponent, {
        width: '250px',
    });

  }

}
