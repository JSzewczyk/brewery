import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatButtonToggleChange} from '@angular/material/button-toggle';
import {ThemeService} from '../../../../commons/services/theme.service';
import {FormControl, Validators} from '@angular/forms';
import {TableColumnsEnum} from '../../../../commons/models/enums/table-columns.enum';
import {MatSelectChange} from '@angular/material/select';
import {BreweryService} from '../../../../commons/requests/brewery.service';
import {BreweryDetailsService} from '../../../../commons/services/brewery-details.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-options-dialog',
  templateUrl: './options-dialog.component.html',
  styleUrls: ['./options-dialog.component.scss']
})
export class OptionsDialogComponent implements OnInit, OnDestroy {

  public readonly TableColumnsEnum = TableColumnsEnum;

  public pageCountControl = new FormControl(15, [Validators.min(15), Validators.max(30)]);
  public tableColumns: string[] = [TableColumnsEnum.NAME, TableColumnsEnum.TYPE, TableColumnsEnum.STATE];
  private $destroy = new Subject();

  constructor(
    private themeService: ThemeService,
    private breweryService: BreweryService,
    private breweryDetailsService: BreweryDetailsService
  ) { }

  ngOnInit(): void {
    this.getBreweryPageNumber();
  }

  public toogleTheme(val: MatButtonToggleChange): void {
    this.themeService.setTheme(val.value);
  }

  public controlChange(): void {
    if (this.pageCountControl.valid) {
      this.breweryDetailsService.setBreweryPageNumber(this.pageCountControl.value);
    }
  }

  public getBreweryPageNumber(): void {
    this.breweryDetailsService.getBreweryPageNumber().pipe(takeUntil(this.$destroy)).subscribe((val: number) => {
      this.pageCountControl.setValue(val);
    });
  }

  public selectionChange(val: MatSelectChange): void {
    this.breweryService.getBreweries(undefined, undefined, val.value);
  }

  ngOnDestroy(): void {
    this.$destroy.next();
  }
}
