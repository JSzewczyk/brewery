import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {BreweryModule} from '../brewery/brewery.module';
import {AppRoutingModule} from '../../app-routing.module';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import { ToolbarComponent } from './toolbar/toolbar.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {OptionsDialogComponent} from './toolbar/options-dialog/options-dialog.component';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';

const materialModules = [
  MatSidenavModule,
  MatToolbarModule,
  MatDialogModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatInputModule,
  MatSelectModule
];

@NgModule({
  declarations: [
    DashboardComponent,
    ToolbarComponent,
    OptionsDialogComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    BreweryModule,
    ReactiveFormsModule,
    materialModules
  ]
})
export class DashboardModule { }
