export enum TableColumnsEnum {
  NAME = 'name',
  TYPE = 'type',
  STATE = 'state'
}
