import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Brewery} from '../models/brewery';

@Injectable({
  providedIn: 'root'
})
export class BreweryService {

  private type = '';
  private page = 15;
  private sort = 'name';

  constructor(
    private http: HttpClient
  ) { }

  public getBreweries(typeInput?: string, pageInput?: number, sortInput?: string): Observable<Brewery[]> {
    let type = typeInput;
    let page = pageInput;
    let sort = sortInput;
    if (!type) { type = this.type; }
    if (!page) { page = this.page; }
    if (!sort) { sort = this.sort; }
    return this.http.get(
    'https://api.openbrewerydb.org/breweries?by_type=' + type + '&per_page=' + page + '&sort=+' + sort
    ) as Observable<Brewery[]>;
  }
}
