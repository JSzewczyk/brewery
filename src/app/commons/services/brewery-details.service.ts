import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BreweryDetailsService {

  private breweryPageNumber: BehaviorSubject<number> = new BehaviorSubject<number>(15);

  constructor() { }

  public setBreweryPageNumber(val: number): void {
    this.breweryPageNumber.next(val);
  }

  public getBreweryPageNumber(): Observable<number> {
    return this.breweryPageNumber.asObservable();
  }
}
