import {Routes} from '@angular/router';
import {DashboardComponent} from '../app/modules/dashboard/dashboard.component';
import {BreweryComponent} from '../app/modules/brewery/brewery.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/brewery',
    pathMatch: 'full'
  },
  {
    path: '',
    component: DashboardComponent,
    children: [
      {path: 'brewery', component: BreweryComponent}
    ]
  }
];
